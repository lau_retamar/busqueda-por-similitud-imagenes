
//const axios = require("axios");

function LoadFile(event, elementohtml) {
    var output = document.getElementById(elementohtml);
    output.src = URL.createObjectURL(event.target.files[0]);

    output.onload = function () {
        //URL.revokeObjectURL(output.src) // free memory
        console.log('Imagen cargada: ' + output.src)

    }
};


function ObtenerBitmap(img) {

    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    
    variableGlobal = ctx.getImageData(0, 0, img.width, img.height).data;
    
    
    
};


function ObtenerBlob(img) {

    var blob = img.src;

    return blob;

};

function ConvertirAB64(texto) {
    return btoa(texto);
};

function Redirigir() {
    document.location.href = '/consultar/';

};

const resize = function (image) {
    return new Promise(function (resolve, reject) {
        const reader = new FileReader();

        // Read the file
        reader.readAsDataURL(image);

        // Manage the `load` event
        reader.addEventListener('load', function (e) {
            // Create new image element
            const ele = new Image();
            ele.addEventListener('load', function () {
                // Create new canvas
                const canvas = document.createElement('canvas');

                // Draw the image that is scaled to `ratio`
                const context = canvas.getContext('2d');
                const w = 100;
                const h = 100;
                canvas.width = w;
                canvas.height = h;
                context.drawImage(ele, 0, 0, w, h);

                // Get the data of resized image
                ('toBlob' in canvas)
                    ? canvas.toBlob(function (blob) {
                        resolve(blob);
                    })
                    : resolve(dataUrlToBlob(canvas.toDataURL()));
            });

            // Set the source
            ele.src = e.target.result;
        });

        reader.addEventListener('error', function (e) {
            reject();
        });
    });
};

const dataUrlToBlob = function (url) {
    const arr = url.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const str = atob(arr[1]);
    let length = str.length;
    const uintArr = new Uint8Array(length);
    while (length--) {
        uintArr[length] = str.charCodeAt(length);
    }
    return new Blob([uintArr], { type: mime });
};

function Redimension100x100(){
    const image = document.getElementById('fileinput').files[0];
    resize(image).then(function(blob){
        let blobChico = URL.createObjectURL(blob);
        console.log('variable blobChico: '+blobChico);
        console.log('variable blob: '+blob);
        let b64deBlob = btoa(blobChico);


        //cambio de link
        document.location.href = '/consultar/' + b64deBlob;
    })

}

async function Consultar(){
    
    let res = await fetch('http://localhost:8001/consultar', {
        method: 'POST',
        
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        body: JSON.stringify({data: ConvertirAB64(variableGlobal)})
        },
    

    )
    
    console.log(res);
};

    
