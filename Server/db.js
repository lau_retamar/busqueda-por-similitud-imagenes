const {Pool} = require('pg')

const pool = new Pool({
    host: 'localhost',
    port: '5432',
    user: 'postgres',
    password: 'sql',
    database: 'frutas', 

})

async function GetTipoFrutaMuestra(bitmap){
    const client = await pool.connect();
    const data = await client.query('SELECT id FROM frutasmuestra WHERE FuncionDistancia(bitmap, frutasmuestra.bm)');
    return data.rows;
    
}

async function GuardarImagen(bitmap){
    const client = await pool.connect();
    const data = await client.query('const client = await pool.connect();')
    await client.query(`INSERT INTO frutasmuestra(bitmap) VALUES(array{${bitmap}});`);
    
}

async function GetTipoFrutaMuestra(){
    const client = await pool.connect();
    const data = await client.query('SELECT * FROM frutasmuestra');
    return data.rows;
    
}


module.exports = {GetTipoFrutaMuestra};